# SHARK for users

Shark is a cross-platform tool for apdating apps. 
To run it you need just a working `tool` specified in your `$path` on your device, `data.csv` file in current folder and you need to know `domain`, `clientId` and be ready to set token when it is needed.

## Run the tool

The usage of this tool you can find running the sommand `$ tool run --help`. You will retrieve the following text:

```
NAME:
   tool run - starts the tool

USAGE:
   tool run [command options] [arguments...]

DESCRIPTION:
   reads data from data fite by --f path using --opt (options file) or --opt.domain, --opt.token, --cpu-num

OPTIONS:
   --cpu-num value       Number of paralel processes. Maximum is 8 (default: 1)
   -f value              Path to the .csv file with MAC addresses data (default: "./data.csv")
   --opt value           Path to the .json options file. Overrides other options flags if defined and exists. Fills from option flags if defined but not exists
   --opt.domain value    Domain path" (default: "https://example.com")
   --opt.clientId value  Client id string (default: "123456789")
   --opt.token value     Token string (default: "123456789")
   --log-lvl value       Log level DEBUG/INFO/WARNING/ERROR/FATAL (default: "WARNING")
```

To run this tool use following command:

```
$ tool run --opt.domain https://mydomain.com --opt.clientId 12345678
```

If you need to specifie .csv file at any path with data (defauld is ./data.csv) just add `-f flag`:

```
$ tool run --opt.domain https://mydomain.com --opt.clientId 12345678 -f some.path/to/data/file.csv
```

You can alse create a json file with options:

```
{
    "clientId": "12345678",
    "domain": "https://mydomain.com"
}
```

and use it instead of other options flags:

```
$ tool run --opt /path/to/json/options/file.json
```

If you want to use a json file with options but you don't have an existing one you can run this command which will create this file by path /path/to/json/options/file.json with data from option flags you have set:

```
$ tool run --opt /path/to/json/options/file.json --opt.domain https://mydomain.com --opt.clientId 12345678
```

## How to get more info:

Use log levels DEBUG/INFO:

```
$ tool run --opt /path/to/json/options/file.json --log-lvl=DEBUG
```