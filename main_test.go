package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewCliApp(t *testing.T) {
	underTest := NewCliApp()
	err := underTest.Run([]string{"./tool", "run", "--opt=./test-data/opts.json", "-f", "./test-data/data.csv"})
	require.NoError(t, err)
}
