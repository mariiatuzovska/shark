# Shark

```
NAME:
   shark - command line client

USAGE:
   tool [global options] command [command options] [arguments...]

VERSION:
   0.0.1

DESCRIPTION:
   updating tool using data from .csv file

AUTHOR:
   Tuzovska Mariia <mariia.tuzovska@gmail.com>

COMMANDS:
   run, start, s, r  starts the tool
   help, h           Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

## run command usage

```
NAME:
   tool run - starts the tool

USAGE:
   tool run [command options] [arguments...]

DESCRIPTION:
   reads data from data fite by --f path using --opt (options file) or --opt.domain, --opt.token, --cpu-num

OPTIONS:
   --cpu-num value       Number of paralel processes. Maximum is 8 (default: 1)
   -f value              Path to the .csv file with MAC addresses data (default: "./data.csv")
   --opt value           Path to the .json options file. Overrides other options flags if defined and exists. Fills from option flags if defined but not exists
   --opt.domain value    Domain path" (default: "https://example.com")
   --opt.clientId value  Client id string (default: "123456789")
   --opt.token value     Token string (default: "123456789")
   --log-lvl value       Log level DEBUG/INFO/WARNING/ERROR/FATAL (default: "WARNING")
```