package main

import (
	"runtime"
	"time"
)

const (
	// defaultTimeout defines default timeout for HTTP requests.
	defaultTimeout = time.Second * 5
	// maxRetry is a number of maximum retries for each request
	maxRetry = 10

	// internal codes
	noErrOccur          = 0
	cannotBuildRequest  = 0x8000
	cannotDoHttpRequest = 0x8001
	tryAgain            = 0x8002
	internalServerError = 0x8003
	unknownStatusCode   = 0x8004
	maxRetryFailed      = 0x8005
	notFound            = 0x8006
	skipTokenInput      = 0x8007
)

var (
	defaultRuntimeProcs = runtime.GOMAXPROCS(0)
)
