package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/mariiatuzovska/logger"
	"github.com/urfave/cli"
)

var log logger.LoggerService

func main() {
	app := NewCliApp()
	app.Run(os.Args)
}

func NewCliApp() *cli.App {
	app := cli.NewApp()
	app.Name = "shark"
	app.Usage = "command line client"
	app.Description = "updating tool using data from .csv file"
	app.Version = "0.0.1"
	app.Authors = []cli.Author{{Name: "Tuzovska Mariia", Email: "mariia.tuzovska@gmail.com"}}
	app.Commands = []cli.Command{
		{
			Name:        "run",
			Usage:       "starts the tool",
			Description: "reads data from data fite by --f path using --opt (options file) or --opt.domain, --opt.token, --cpu-num",
			Aliases:     []string{"start", "s", "r"},
			Action: func(c *cli.Context) error {
				logLvl := getLogLevel(c.String("log-lvl"))
				log = logger.NewLoggerService().SetServiceName(app.Name).SetLevel(logLvl)
				opts, err := NewOptions(c.String("opt"), c.String("opt.domain"), c.String("opt.clientId"), c.String("opt.token"), c.Int("cpu-num"))
				if err != nil {
					log.Fatalf("%v", 1, err)
				}
				data, err := NewDataFromCsv(c.String("f"))
				if err != nil {
					log.Fatalf("%v", 1, err)
				}
				if err := NewTool(os.Stdin, opts).Run(data); err != nil {
					log.Fatalf("%v", 1, err)
				}
				return nil
			},
			Flags: []cli.Flag{
				&cli.IntFlag{
					Name:  "cpu-num",
					Usage: fmt.Sprintf("Number of paralel processes. Maximum is %d", defaultRuntimeProcs),
					Value: 1,
				},
				&cli.StringFlag{
					Name:  "f",
					Usage: `Path to the .csv file with MAC addresses data`,
					Value: "./data.csv",
				},
				&cli.StringFlag{
					Name:  "opt",
					Usage: `Path to the .json options file. Overrides other options flags if defined and exists. Fills from option flags if defined but not exists`,
				},
				&cli.StringFlag{
					Name:  "opt.domain",
					Usage: `Domain path"`,
					Value: "https://example.com",
				},
				&cli.StringFlag{
					Name:  "opt.clientId",
					Usage: `Client id string`,
					Value: "123456789",
				},
				&cli.StringFlag{
					Name:  "opt.token",
					Usage: `Token string`,
					Value: "123456789",
				},
				&cli.StringFlag{
					Name:  "log-lvl",
					Usage: "Log level DEBUG/INFO/WARNING/ERROR/FATAL",
					Value: "WARNING",
				},
			},
		},
	}
	return app
}

func getLogLevel(lvl string) int {
	switch strings.ToUpper(lvl) {
	case "DEBUG":
		return logger.DebugLevel
	case "INFO":
		return logger.InfoLevel
	case "WARNING":
		return logger.WarningLevel
	case "ERROR":
		return logger.ErrorLevel
	case "FATAL":
		return logger.FatalLevel
	default:
		return logger.DefaultLevel
	}
}
