package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync"
)

type Tool interface {
	Run(data [][]string) error
	CreateRequest(macAddress string) (*http.Request, error)
	Do(macAddress string) int
}

type tool struct {
	input  io.Reader
	client interface {
		Do(req *http.Request) (*http.Response, error)
	}
	opts            *Options
	updateTokenSync chan struct{}
}

func NewTool(in io.Reader, opts *Options) Tool {
	client := &http.Client{Timeout: defaultTimeout}
	return &tool{in, client, opts, make(chan struct{}, 1)}
}

// Run starts doing async requests for apdating app. Fails only if gets not null status codes exept maxRetryFailed, notFound.
func (t *tool) Run(data [][]string) error {
	wg := sync.WaitGroup{}
	wg.Add(len(data))
	log.Infof("stating with %d parallel process(es)", t.opts.RuntimeProcs)
	limitProcs := make(chan struct{}, t.opts.RuntimeProcs)
	for index := range data {
		go func(i int) {
			defer wg.Done()
			if len(data[i]) == 0 {
				return
			}
			macAddress := data[i][0]
			limitProcs <- struct{}{}
			code := t.Do(macAddress)
			switch code {
			case noErrOccur:
				log.Infof("%s: sucessfuly updated", macAddress)
			case maxRetryFailed:
				log.Warningf("%s: max retry failed", macAddress)
			case notFound:
				log.Warningf("%s: not found", macAddress)
			default:
				log.Fatalf("finishing with status code %d", code, code)
			}
			<-limitProcs
		}(index)
	}
	wg.Wait()
	return nil
}

// CreateRequest returns new http request with required headers
func (t *tool) CreateRequest(macAddress string) (*http.Request, error) {
	body := createBody()
	endpoint := t.opts.Domain + "/profiles/" + t.opts.ClientId + ":" + macAddress
	request, err := http.NewRequest(http.MethodPut, endpoint, bytes.NewReader(body))
	if err != nil {
		log.Errorf("cannot build request for mac address %s: %v", macAddress, err)
		return nil, err
	}
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("x-client-id", t.opts.ClientId)
	request.Header.Add("x-authentication-token", t.opts.Token)
	return request, nil
}

// createBody can be modified but in this reqlisation just used as is
func createBody() []byte {
	return []byte(`{
		"profile": {    
			"applications": [
				{
					"applicationId": "music_app"
					"version": "v1.4.10"
				},
				{
					"applicationId": "diagnostic_app",
					"version": "v1.2.6"
				},
				{
					"applicationId": "settings_app",
					"version": "v1.1.5"
				}
			]
		}
	}`)
}

// Do tries to do requests and checks response.
// If 200 - successfully finishes with OK status code.
// If 401 - synchronizes async processes and gets new token if needed. Tries to do request one more time.
// If 409 - runs one mor time. Max retries is 10.
// If 404 - skips this mac address.
// If 500 or unknown code - finishes with not null status code.
func (t *tool) Do(macAddress string) int {
	for i := 0; i < maxRetry; i++ {
		request, err := t.CreateRequest(macAddress)
		if err != nil {
			log.Errorf("%s: cannot build request for mac address", macAddress)
			return cannotBuildRequest
		}
		resp, err := t.client.Do(request)
		if err != nil {
			log.Errorf("%s: cannot do request for mac address: %v", macAddress, err)
			return cannotDoHttpRequest
		}
		switch resp.StatusCode {
		case http.StatusOK:
			return noErrOccur
		case http.StatusUnauthorized:
			code, err := t.tryAgainOrUpdateToken(macAddress)
			if err != nil {
				return code
			}
			switch code {
			case skipTokenInput, http.StatusOK:
				return code
			case tryAgain:
				// to be sure that we will do request one more time
				if i == maxRetry-1 {
					i--
				}
			}
		case http.StatusNotFound:
			return notFound
		case http.StatusConflict:
			log.Warningf("%s: conflict", macAddress)
			continue
		case http.StatusInternalServerError:
			return internalServerError
		default:
			return unknownStatusCode
		}
	}
	return maxRetryFailed
}

func NewDataFromCsv(filePath string) ([][]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("cannot open %s csv file: %v", filePath, err)
	}
	defer file.Close()
	fileReader := csv.NewReader(file)
	records, err := fileReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("cannot read %s csv file: %v", filePath, err)
	}
	if len(records) == 0 {
		log.Fatalf("%s data file is empty", 1, filePath)
	}
	log.Infof("csv data successfully read from file %s", filePath)
	if len(records[0]) != 0 {
		// skip the title line
		if strings.ToLower(records[0][0]) == "mac_addresses" {
			records = records[1:]
		}
	}
	return records, nil
}

// tryAgainOrUpdateToken synchronizes and tries to do one more request to the server. If gets 401 then asks for a new token.
func (t *tool) tryAgainOrUpdateToken(macAddress string) (int, error) {
	if t.input == nil {
		return skipTokenInput, nil
	}
	t.updateTokenSync <- struct{}{}
	defer func() {
		<-t.updateTokenSync
	}()
	request, err := t.CreateRequest(macAddress)
	if err != nil {
		log.Errorf("%s: cannot build request for mac address", macAddress)
		return cannotBuildRequest, err
	}
	resp, err := t.client.Do(request)
	if err != nil {
		log.Errorf("%s: cannot do request for mac address: %v", macAddress, err)
		return cannotDoHttpRequest, err
	}
	if resp.StatusCode != http.StatusNotFound {
		return resp.StatusCode, nil
	}
	log.Info("current token is expired or broken and new token has to be set")
	t.readToken()
	return tryAgain, nil
}

func (t *tool) readToken() {
	fmt.Print("Current token is expired or broken. Please write new token:\n")
	reader := bufio.NewReader(t.input)
	token, err := reader.ReadString('\n')
	if err != nil {
		log.Fatalf("cannot read string from console: %v", 1, err)
	}
	t.opts.Token = strings.Trim(token, "\n")
}
