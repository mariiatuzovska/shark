package main

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"testing"

	"github.com/mariiatuzovska/logger"
	"github.com/stretchr/testify/require"
)

type MockHttpClient struct {
	response *http.Response
	err      error
}

func (m *MockHttpClient) Do(req *http.Request) (*http.Response, error) {
	return m.response, m.err
}

func TestDo(t *testing.T) {
	cases := []struct {
		name       string
		opts       *Options
		mockClient interface {
			Do(req *http.Request) (*http.Response, error)
		}
		macAddress         string
		expectedStatusCode int
	}{
		{
			name: "happy path",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: &http.Response{
					Status:     "200 OK",
					StatusCode: 200,
					Body: io.NopCloser(bytes.NewReader([]byte(`{
						"profile": {},
					  }`))),
				},
				err: nil,
			},
			macAddress:         "12345",
			expectedStatusCode: noErrOccur,
		},
		{
			name: "unknown status code",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: &http.Response{
					Status:     "400 OK",
					StatusCode: 400,
					Body: io.NopCloser(bytes.NewReader([]byte(`{
						"profile": {},
					  }`))),
				},
				err: nil,
			},
			macAddress:         "12345",
			expectedStatusCode: unknownStatusCode,
		},
		{
			name: "internal server error",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: &http.Response{
					Status:     "500 OK",
					StatusCode: 500,
					Body: io.NopCloser(bytes.NewReader([]byte(`{
						"profile": {},
					  }`))),
				},
				err: nil,
			},
			macAddress:         "12345",
			expectedStatusCode: internalServerError,
		},
		{
			name: "conflict",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: &http.Response{
					Status:     "409 OK",
					StatusCode: 409,
					Body: io.NopCloser(bytes.NewReader([]byte(`{
						"profile": {},
					  }`))),
				},
				err: nil,
			},
			macAddress:         "12345",
			expectedStatusCode: maxRetryFailed,
		},
		{
			name: "unauthorized",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: &http.Response{
					Status:     "401 OK",
					StatusCode: 401,
					Body: io.NopCloser(bytes.NewReader([]byte(`{
						"profile": {},
					  }`))),
				},
				err: nil,
			},
			macAddress:         "12345",
			expectedStatusCode: skipTokenInput,
		},
		{
			name: "cannot do request",
			opts: &Options{
				Domain:   "test.com",
				ClientId: "123",
			},
			mockClient: &MockHttpClient{
				response: nil,
				err:      errors.New("error"),
			},
			macAddress:         "12345",
			expectedStatusCode: cannotDoHttpRequest,
		},
	}
	log = logger.NewLoggerService().SetServiceName("shark_test").SetLevel(logger.DebugLevel)
	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			underTest := &tool{
				client: tc.mockClient,
				opts:   tc.opts,
			}
			code := underTest.Do(tc.macAddress)
			require.Equal(t, tc.expectedStatusCode, code)
		})
	}
}
