package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
)

type Options struct {
	Domain       string `json:"domain"`
	ClientId     string `json:"clientId"`
	Token        string `json:"token"`
	RuntimeProcs int    `json:"cpuNum"`
}

func NewOptions(filePath, domain, clientId, token string, runtimeProcs int) (*Options, error) {
	opts := &Options{
		Domain:       domain,
		ClientId:     clientId,
		Token:        token,
		RuntimeProcs: runtimeProcs,
	}
	if filePath != "" {
		log.Debug("opt flag is not nil, trying to read options file")
		_, err := os.Stat(filePath)
		if os.IsNotExist(err) {
			log.Debugf("options file doesn't exist. creating new options file by path %s", filePath)
			file, err := os.Create(filePath)
			if err != nil {
				return nil, fmt.Errorf("cannot create options file: %v", err)
			}
			err = file.Close()
			if err != nil {
				return nil, fmt.Errorf("cannot create options file: %v", err)
			}
		}

		file, err := os.OpenFile(filePath, os.O_RDWR, os.ModePerm)
		if err != nil {
			return nil, fmt.Errorf("cannot open options file: %v", err)
		}
		defer file.Close()

		rawData, err := os.ReadFile(filePath)
		if err != nil {
			return nil, fmt.Errorf("cannot read options file: %v", err)
		}
		if len(rawData) == 0 {
			// file is empty and has to be filled
			if err := opts.Validate(); err != nil {
				return nil, err
			}
			rawData, err = json.MarshalIndent(opts, "", "	")
			if err != nil {
				return nil, fmt.Errorf("cannot convert option flags to json: %v", err)
			}
			_, err := file.Write(rawData)
			if err != nil {
				return nil, fmt.Errorf("cannot write option flags to json file: %v", err)
			}
			log.Infof("%s options file successfully created and filled from flags", filePath)
			return opts, nil
		}
		// overriding optins from file entry
		if err := json.Unmarshal(rawData, opts); err != nil {
			log.Error("options file has not nil data but cannot be parsed")
			return nil, fmt.Errorf("cannot parse options file: %v", err)
		}
		log.Infof("%s options file is successfully read and oprion flags were overrided", filePath)
	} else {
		log.Infof("options were set from flags")
	}
	if err := opts.Validate(); err != nil {
		return nil, err
	}
	log.Debugf("starting with options %#v", *opts)
	return opts, nil
}

func (o *Options) Validate() error {
	if o.Domain == "" {
		return errors.New("domain must be set")
	}
	if o.ClientId == "" {
		return errors.New("clientId must be set")
	}
	if o.RuntimeProcs <= 0 || o.RuntimeProcs >= defaultRuntimeProcs {
		o.RuntimeProcs = 1
	}
	return nil
}
