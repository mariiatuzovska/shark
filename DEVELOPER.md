# SHARK for developers

## Build

Requirments:
* GO 1.19

To build shark tool run command from current folder:

```
$ go build -o ./tool
```

## Run tests

To run tests run the following command:

```
$ go test -timeout 30s
```